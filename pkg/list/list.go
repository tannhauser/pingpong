package list

import "sync"

// List defines the ping/pong buffer for boolean values.
// Can be used for cache control such as deduplication of work queue tasks to avoid re-execution of WIP tasks.
type List struct {
	pingList    map[string]bool
	pongList    map[string]bool
	isPing      bool
	isRunning   bool
	routineLock sync.Mutex
	queue       chan *listTask
	exit        chan struct{}
	*Options
	sync.Mutex
}

// New returns a brand new Bool structure.
func New(opts ...OptFunc) *List {
	o := newOptions()
	for _, opt := range opts {
		opt(o)
	}
	return &List{
		pingList: make(map[string]bool),
		pongList: make(map[string]bool),
		isPing:   true,
		queue:    make(chan *listTask, o.QueueSize()),
		exit:     make(chan struct{}),
		Options:  o,
	}
}

// ping returns if the active list is pingList.
func (l *List) ping() bool {
	return (*l).isPing
}

// running returns if the list management routine is running.
func (l *List) running() bool {
	return (*l).isRunning
}

// swap swaps the ping/pong buffer.
func (l *List) swap() {
	l.Lock()
	defer l.Unlock()
	l.isPing = l.ping() != true
}

// active returns the reference to the active list.
func (l *List) active() *map[string]bool {
	if l.ping() {
		return &(*l).pingList
	}
	return &(*l).pongList
}

// inactive returns the reference to the inactive list.
func (l *List) inactive() *map[string]bool {
	if l.ping() {
		return &(*l).pongList
	}
	return &(*l).pingList
}

// OnList returns if the given key is on the list.
func (l *List) OnList(key string) bool {
	l.Lock()
	defer l.Unlock()
	list := l.active()
	_, on := (*list)[key]
	// dereference the list
	list = nil
	return on
}

// enlist enlists the given key.
func (l *List) enlist(key string) {
	list := l.inactive()
	(*list)[key] = true
	l.swap()
	list = l.inactive()
	(*list)[key] = true
	list = nil
	return
}

// delist removes the given key from the lists.
func (l *List) delist(key string) {
	list := l.inactive()
	if _, ok := (*list)[key]; ok {
		delete(*list, key)
	}
	l.swap()
	list = l.inactive()
	if _, ok := (*list)[key]; ok {
		delete(*list, key)
	}
	list = nil
	return
}

// noop defines a no operation function for default cases.
func (l *List) noop() {}

// routine performs the background routine for task dispatch
func (l *List) routine() {
	for {
		select {
		case t := <-(*l).queue:
			switch (*t).op {
			case opEnlist:
				l.enlist((*t).key)
			case opDelist:
				l.delist((*t).key)
			default:
				l.noop()
			}
			if (*t).Wait != nil {
				t.Done()
			}
		case <-(*l).exit:
			(*l).routineLock.Lock()
			defer (*l).routineLock.Unlock()
			(*l).isRunning = false
			return
		}
	}
}

// run checks if the routine is running, if not, run the routine in background.
func (l *List) run() {
	(*l).routineLock.Lock()
	defer (*l).routineLock.Unlock()
	if l.running() {
		return
	}
	go l.routine()
	(*l).isRunning = true
	return
}

func (l *List) insertTask(op int, key string) *Wait {
	if !l.running() {
		l.run()
	}
	var wait *Wait
	if l.HasWait() {
		wait = NewWait()
	}
	task := &listTask{
		op:   op,
		key:  key,
		Wait: wait,
	}
	(*l).queue <- task
	return wait
}

// Enlist enlist the key on the lists.
func (l *List) Enlist(key string) *Wait {
	return l.insertTask(opEnlist, key)
}

// Delist removes the key from the lists.
func (l *List) Delist(key string) *Wait {
	return l.insertTask(opDelist, key)
}
