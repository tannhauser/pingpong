package list

import "encoding/binary"
import "encoding/hex"
import "sync"
import "time"
import "testing"

const byteCnt = 4
const loopCnt = 1000000

func getString(i int) string {
	bs := make([]byte, 8)
	binary.LittleEndian.PutUint64(bs, uint64(i))
	return hex.EncodeToString(bs)
}

func (l *List) testRoutine(t *testing.T, index int, wg *sync.WaitGroup) {
	defer (*wg).Done()
	key := getString(index)
	done := l.Enlist(key)
	done.Wait()
	if on := l.OnList(key); !on {
		t.Errorf("[%d] %s enlisted but not on list\n", index, key)
		return
	}
	done = l.Delist(key)
	done.Wait()
	if on := l.OnList(key); on {
		t.Errorf("[%d] %s delisted but still on list\n", index, key)
		return
	}
	return
}

func Test(t *testing.T) {
	t.Logf("start testing...\n")
	list := New(WithQueueSize(2), WithWait(true))
	wg := sync.WaitGroup{}
	start := time.Now().UTC()
	for i := 0; i < loopCnt; i++ {
		wg.Add(1)
		go list.testRoutine(t, i, &wg)
	}
	wg.Wait()
	duration := time.Since(start)
	ops := loopCnt / duration.Seconds()
	t.Logf("%d rounds finished in %v, %f operations/sec\n", loopCnt, duration, ops)
	return
}
