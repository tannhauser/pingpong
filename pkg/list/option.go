package list

// default values
const (
	DefaultDebug     = false
	DefaultQueueSize = 4
	defaultTesting   = false
)

// Options defines the list options.
type Options struct {
	debug     bool
	hasWait   bool
	queueSize int
}

func newOptions() *Options {
	return &Options{
		debug:     DefaultDebug,
		queueSize: DefaultQueueSize,
	}
}

// Debug returns if the debug mode is activated.
func (o *Options) Debug() bool {
	return (*o).debug
}

// QueueSize returns the current QueueSize.
func (o *Options) QueueSize() int {
	return (*o).queueSize
}

// HasWait returns if the list will return Wait handler.
func (o *Options) HasWait() bool {
	return (*o).hasWait
}

// OptFunc defines the option configuration functions.
type OptFunc func(*Options)

// WithDebug implements the OptFunc for debug mode configuration.
func WithDebug(d bool) OptFunc {
	return func(o *Options) {
		(*o).debug = d
	}
}

// WithQueueSize implements the OptFunc for list task queue size configuration.
func WithQueueSize(s int) OptFunc {
	return func(o *Options) {
		(*o).queueSize = s
	}
}

// WithWait implements the OptFunc for Wait handler return.
func WithWait(w bool) OptFunc {
	return func(o *Options) {
		(*o).hasWait = w
	}
}
