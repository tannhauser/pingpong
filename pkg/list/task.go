package list

const (
	opEnlist = iota
	opDelist = iota
)

// listTask defines the task for list management.
type listTask struct {
	op  int
	key string
	*Wait
}
