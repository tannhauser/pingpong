package list

// Wait defines the wait handler for enlist/delist.
type Wait struct {
	done chan struct{}
}

// NewWait creates and returns a new Wait handler.
func NewWait() *Wait {
	return &Wait{
		done: make(chan struct{}, 1),
	}
}

// Wait waits for the enlist/delist to be finished.
func (w *Wait) Wait() {
	<-(*w).done
}

// Done finishes the waiting.
func (w *Wait) Done() {
	(*w).done <- struct{}{}
}
